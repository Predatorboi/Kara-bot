import discord
from discord.ext import commands
from secret import bot_token
from convert import convert
from gifs import hugs
from random import choice, randrange

client = commands.Bot(command_prefix="//", description="Wszystkie komendy bota:")
allowedusers = []


@client.event
async def on_ready():
    print("Initialized user")
    print("Logged in as {}".format(client.user.name))
    print("Client ID: {}".format(client.user.id))
    print("Command prefix: {}".format(client.command_prefix))
    print("Client ready {}".format("-" * 40))

    laststatus = open("laststatus.txt", "r")
    game = discord.Game(name=laststatus.read())
    await client.change_presence(game=game)


@client.event
async def on_message(message):
    permissions = message.channel.permissions_for(message.author)
    # if permissions.manage_messages:
    if permissions.administrator:
        await client.process_commands(message)


@client.event
async def on_member_join(member):
    channel = discord.Object("483273472555089930")
    greetchannels = [client.get_channel("466728190904369153"), client.get_channel("464482946254897191")]
    await client.send_message(channel, "Witamy na serwerze, **{}**. Zapoznaj się z {} i dodaj sobie {}".format(member.name, greetchannels[0].mention, greetchannels[1].mention))
    # TODO:Make server specific


@client.event
async def on_member_remove(member):
    channel = discord.Object("483273472555089930")
    await client.send_message(channel, "**{}** wychodzi z serwera.".format(member.name))


@client.command(pass_context=True)
async def clear(ctx, amount=1):
    """Usuwa podaną ilość wiadomości na kanale (domyślnie 1 wiadomość)"""
    channel = ctx.message.channel
    messages = []
    amount = convert(amount, int)

    if amount == 0:
        await client.say("Can't delete less that 1 message")
    else:
        async for message in client.logs_from(channel, limit=int(amount + 1)):
            messages.append(message)
        await client.delete_messages(messages)


@client.command(pass_context=True)
async def role(ctx, *args):
    """Pokazuje informacje o danej roli (nazwa)"""
    if not args:
        await client.say("Podaj rolę, o której chcesz zobaczyć informacje. Przykład: `{}role Żółty`".format(client.command_prefix))
    else:
        role = discord.utils.get(ctx.message.channel.server.roles, name=args[0])
        if not role:
            await client.say("Rola o nazwie `{}` nie została znaleziona.".format(args[0]))
        else:
            author = await client.get_user_info(ctx.message.author.id)

            embed = discord.Embed(title="Informacje o roli", color=role.color)
            embed.add_field(name="Nazwa", value=role.name, inline=True)
            embed.add_field(name="Data utworzenia", value=str(role.created_at)[0:19], inline=True)
            embed.add_field(name="ID", value=role.id, inline=True)
            embed.set_footer(text=ctx.subcommand_passed, icon_url=author.avatar_url)
            await client.say(embed=embed)


@client.command(pass_context=True)
async def roleme(ctx, *args):
    """Dodaje wybraną rolę (nazwa)"""
    if not args:
        await client.say("Podaj rolę, którą chcesz otrzymać. Przykład: `{}roleme Żółty`".format(client.command_prefix))
    else:
        role = discord.utils.get(ctx.message.channel.server.roles, name=args[0])
        if not role:
            await client.say("Rola o nazwie **{}** nie została znaleziona.".format(args[0]))
        else:
            if role in ctx.message.author.roles:
                await client.remove_roles(ctx.message.author, role)
                await client.say("Zabrano rolę **{}** użytkownikowi **{}**.".format(role, ctx.message.author.name))
            else:
                await client.add_roles(ctx.message.author, role)
                await client.say("Dodano rolę **{}** użytkownikowi **{}**.".format(role, ctx.message.author.name))


@client.command(pass_context=True)
async def user(ctx, *args):
    """Pokazuje informacje o danym użytkowniku (@wzmianka, nazwa lub pusto)"""
    author = await client.get_user_info(ctx.message.author.id)
    if not args:
        member = discord.utils.get(ctx.message.server.members, name=ctx.message.author.name)
    else:
        if str(args[0]).startswith("<@") and str(args[0]).endswith(">"):
            user = await client.get_user_info(str(args[0])[2:20])
            member = discord.utils.get(ctx.message.server.members, name=user.name)
        else:
            member = discord.utils.find(lambda m: m.name == str(args[0]), ctx.message.channel.server.members)

    permissions = ctx.message.channel.permissions_for(member)
    perms = []
    for perm in permissions:
        perms.append(perm)
    # await client.say(perms)
    # if permissions.manage_messages:
    #     await client.say("Can manage messages")
    # if permissions.administrator:
    #     await client.say("Is administrator")

    embed = discord.Embed(title="Informacje o użytkowniku", color=member.color)
    embed.set_thumbnail(url=str(member.avatar_url).replace("?size=1024", "?size=2048"))
    embed.add_field(name="Nazwa użytkownika", value=member.name, inline=True)
    embed.add_field(name="Nazwa serwerowa", value=member.nick, inline=True)
    embed.add_field(name="Data utworzenia konta", value=str(member.created_at)[0:19], inline=True)
    embed.add_field(name="Data dołączenia do serwera", value=str(member.joined_at)[0:19], inline=True)
    embed.add_field(name="ID", value=member.id, inline=True)
    embed.add_field(name="Ilość ról", value=str(len(member.roles)), inline=True)
    embed.set_footer(text=ctx.subcommand_passed, icon_url=author.avatar_url)
    await client.say(embed=embed)


@client.command(pass_context=True)
async def avatar(ctx, *args):
    """Pokazuje avatar wskazanego użytkownika (@wzmianka lub ID)"""
    author = await client.get_user_info(ctx.message.author.id)
    if not args:
        member = discord.utils.get(ctx.message.server.members, name=ctx.message.author.name)
    else:
        if str(args[0]).startswith("<@") and str(args[0]).endswith(">"):
            user = await client.get_user_info(str(args[0])[2:20])
            member = discord.utils.get(ctx.message.server.members, name=user.name)
        else:
            member = discord.utils.find(lambda m: m.name == str(args[0]), ctx.message.channel.server.members)

    embed = discord.Embed(title="Avatar użytkownika {}".format(member.name), color=0xe90fee)
    embed.set_image(url=member.avatar_url.replace("?size=1024", "?size=2048"))
    embed.set_footer(text=ctx.subcommand_passed, icon_url=author.avatar_url)
    await client.say(embed=embed)


@client.command(pass_context=True)
async def hug(ctx, member : discord.Member):
    """Wysyła gif z przytuleniem (@wzmianka)"""
    embed = discord.Embed(description=f"{ctx.message.author.mention} przytulił/a {member.mention}", color=0xe90fee)
    embed.set_image(url=choice(hugs))
    embed.set_footer(text=ctx.subcommand_passed, icon_url=ctx.message.author.avatar_url)
    await client.say(embed=embed)


@client.command(pass_context=True)
async def dev(ctx, *args):
    """Komendy developera"""
    if ctx.message.author.id == "195935967440404480":
        if str(args[0]) == "off":
            await client.delete_message(ctx.message)
            client.logout()
            client.close()
            exit()

        elif str(args[0]) == "status":
            title = ""
            for word in args[1:]:
                title += "{} ".format(str(word))

            with open("laststatus.txt", "w") as last:
                last.write(title)

            await client.change_presence(game=discord.Game(name=title))
            await client.say(f"Zmieniono status na: **{title}**")

        elif str(args[0]) == "addtrust":
            if str(args[1]).startswith("<@") and str(args[1]).endswith(">"):
                with open("trustedusers.txt", "a") as trusted:
                    trusted.write(str(args[1])[2:20] + "\n")
                    await client.say(f"Dodano {str(args[1])} ({str(args[1])[2:20]}) do zaufanych.")

        elif str(args[0]) == "trustlist":
            with open("trustedusers.txt", "r") as trusted:
                # trusted = trusted.split("\n")
                # print(trusted)
                await client.say(f"Lista zaufanych użytkowników (ID): \n{trusted}")

        elif str(args[0]) == "reltrust":
            with open("trustedusers.txt", "r") as trusted:
                trusted.read().split("\n")
                await client.say(trusted)

        elif str(args[0]) == "say":
            message = ""
            for word in args[1:]:
                message += word + " "
            await client.delete_message(ctx.message)
            await client.say(message)

        elif str(args[0]) == "creator":
            await client.say("Moim twórcą jest ＰＯＴＥＺＮＹ Predator#5546.")


@client.command(pass_context=True)
async def butelka(ctx, *args):
    """Losuje osobę z wybranych (@wzmianka @wzmianka)"""
    await client.say(f"{ctx.message.author} kręci :baby_bottle:. Wypadło na {choice(args)}. Prawda czy wyzwanie?")


@client.command(pass_context=True)
async def roll(ctx, *args):
    """Losuje liczbę od 0 do wybranego limitu (liczba)"""
    if int(args[0]) < 2:
        await client.say("Wskaż liczbę powyżej 2")
    else:
        await client.say(f"{ctx.message.author} rzuca :1234:. Wyrzucony numer: {randrange(0, int(args[0]))}")


@client.command()
async def ping():
    """Pokazuje ping bota do serwera"""
    await client.say("Pong! :ping_pong:")


client.run(bot_token)
