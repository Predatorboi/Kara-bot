@client.command(pass_context=True)
async def user(ctx, *args):
    """Pokazuje informacje o danym użytkowniku (@wzmianka lub ID)"""
    if not args:
        await client.say("Potrzebuję osoby o której pokażę informacje. Przykład: `{}user @Predator`".format(client.command_prefix))
    else:
        if str(args[0]).startswith("<@") and str(args[0]).endswith(">"):
            userid = str(args[0])
            userid = userid[2:20]
        else:
            userid = str(args[0])

        author = await client.get_user_info(ctx.message.author.id)
        user = await client.get_user_info(userid)
        avatar = str(user.avatar_url).replace("?size=1024", "?size=2048")

        embed = discord.Embed(title="Informacje o użytkowniku", color=0xe90fee)
        embed.set_thumbnail(url=avatar)
        embed.add_field(name="Nazwa użytkownika", value=user.name, inline=True)
        embed.add_field(name="Nazwa serwerowa", value=user.display_name, inline=True)
        embed.add_field(name="Data utworzenia konta", value=str(user.created_at)[0:19], inline=True)
        embed.add_field(name="ID", value=user.id, inline=True)
        embed.set_footer(text=author.name, icon_url=author.avatar_url)
        await client.say(embed=embed)
