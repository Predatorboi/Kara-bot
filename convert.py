def convert(input_var, var_format):
    try:
        input_var = var_format(input_var)
        return input_var
    except ValueError:
        pass